#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX 21
#define MAXPRODUCTS 15
#define MAXTX 50

struct TIEMPO {
	int dia;
	int mes;
	int anio;
	int hora;
	int minuto;
};

struct PRODUCTOS {
	int codigo;
	char nombre[MAX];
	char descripcion[MAX];
	char categoria[MAX];
	int invInicial;
	int invActual;
	char usuario[MAX];
	struct TIEMPO tiempo;
};

struct TRANSACCIONES {
	int idtx;
	int codigo;
	int tipo;
	int invAnterior;
	int cantidad;
	int invActual;
	char usuario[MAX];
	struct TIEMPO tiempo;
};

void Reinicio(struct PRODUCTOS *productos,struct TRANSACCIONES *transacciones);
void SystemHour(struct TIEMPO *retTime);
int Menu();
int VerificarExistencia(struct PRODUCTOS *productos,int tempCode);
void IngresoProductos(struct PRODUCTOS *productos,int idprod,struct TRANSACCIONES *transacciones,int idtx);
void GenerarTxIngreso(struct TRANSACCIONES *trans,struct PRODUCTOS *prod,int idprod,int idtx,int tempCode);
void MetodoBurbuja(struct PRODUCTOS *productos,int idprod);
void MostrarDetalles(struct PRODUCTOS *productos);
void MostrarInventario(struct PRODUCTOS *prod);
void Transaccion(struct PRODUCTOS*,struct TRANSACCIONES *,int idtx);
void Tiempo(struct TIEMPO);
void MenuReporte(struct TRANSACCIONES *transacciones,struct PRODUCTOS *productos,int idtx);
void ReporteTxTodos(struct TRANSACCIONES *transacciones,int idtx);
void PedirFecha(int fecha[]);
void ReporteTxFecha(struct TRANSACCIONES *transacciones,int idtx);
void ReporteTxFechaProd(struct TRANSACCIONES *transacciones,struct PRODUCTOS *productos,int idtx);
void Creditos();

int main(){
	struct PRODUCTOS productos[MAXPRODUCTS];
	struct TRANSACCIONES transacciones[MAXTX];
	
	Reinicio(productos,transacciones);
	
	int idtx=0,idprod=0;
	int opcion = 0;
	
	do{
		opcion = Menu();
		switch(opcion){
			case 1://Ingresar nuevo producto
				IngresoProductos(productos,idprod,transacciones,idtx);
				idprod++;
				idtx++;
				MetodoBurbuja(productos,idprod);
				break;
			case 2: //Mostrar detalles de producto
				MostrarDetalles(productos);
				
				break;
			case 3://Mostrar Inventario actual
				MostrarInventario(productos);
				
				break;
			case 4://Realizar Transaccion
				Transaccion(productos,transacciones,idtx);
				idtx++;
				
				break;
			case 5://Reporte de Transacciones de Productos
				MenuReporte(transacciones,productos,idtx);
				
				break;
			case 6://Salir
				
				break;
		}
	}while(opcion!=6);
	
	Creditos();
	
	fflush(stdin);
	getchar();
	
	return 0;
}

void Reinicio(struct PRODUCTOS *productos,struct TRANSACCIONES *transacciones){
	for(int i=0;i<MAXPRODUCTS;i++){
		productos[i].codigo=-1;
		strcpy(productos[i].nombre,"");
		strcpy(productos[i].descripcion,"");
		strcpy(productos[i].categoria,"");
		productos[i].invInicial=0;
		productos[i].invActual=0;
		strcpy(productos[i].usuario,"");
		productos[i].tiempo.dia=0;
		productos[i].tiempo.mes=0;
		productos[i].tiempo.anio=0;
		productos[i].tiempo.hora=0;
		productos[i].tiempo.minuto=0;
	}
	for(int j=0; j<MAXTX; j++){
		transacciones[j].idtx=-1;
		transacciones[j].codigo=0;
		transacciones[j].tipo=0;
		transacciones[j].invAnterior=0;
		transacciones[j].cantidad=0;
		transacciones[j].invActual=0;
		strcpy(transacciones[j].usuario,"");
		transacciones[j].tiempo.dia=0;
		transacciones[j].tiempo.mes=0;
		transacciones[j].tiempo.anio=0;
		transacciones[j].tiempo.hora=0;
		transacciones[j].tiempo.minuto=0;
	}
}

void SystemHour(struct TIEMPO *returnTime){
	time_t tiempoAhora;
	struct tm *miTiempo;
	time(&tiempoAhora);
	miTiempo = localtime(&tiempoAhora);
	
	returnTime->hora   = miTiempo->tm_hour;
	returnTime->minuto = miTiempo->tm_min;
	returnTime->dia    = miTiempo->tm_mday;
	returnTime->mes    = miTiempo->tm_mon+1;
	returnTime->anio   = miTiempo->tm_year+1900;
	
}

int Menu(){
	int opcion;
	do{
		system("cls");
		printf("\n                       PROYECTO - SISTEMA DE INVENTARIO                        \n");
		printf(" ============================================================================= \n");
		printf("\n  Seleccione la opcion que desee realizar:\n");
		printf("\n  1. Ingresar nuevo producto");
		printf("\n  2. Mostrar detalles de producto");
		printf("\n  3. Mostrar Inventario actual");
		printf("\n  4. Realizar Movimiento");
		printf("\n     > Ingreso de Productos");
		printf("\n     > Egreso de Productos");
		printf("\n  5. Reporte de Transacciones");
		printf("\n  6. Salir");
		printf("\n\n Opcion:  [ ]\b\b");
		scanf("%d",&opcion);
		if(opcion<1||opcion>6){
			printf("Opcion invalida, vuela a intentarlo.");
			fflush(stdin);
			getchar();
		}
	}while(opcion<1||opcion>6);
	return opcion;
}

int VerificarExistencia(struct PRODUCTOS *productos,int tempCode){
	int encontrar=-1;
	for(int i=0;i<MAXPRODUCTS;i++){
			if(productos[i].codigo==tempCode){
				encontrar=i;
			}
		}
	return encontrar;
}

void IngresoProductos(struct PRODUCTOS *prod,int idprod,struct TRANSACCIONES *trans,int idtx){
	int encontrar=-1,tempCode=0;
	
	do{
		system("cls");
		fflush(stdin);
		printf("\n                     ==> INGRESO DE NUEVOS PRODUCTOS <==                     \n");
		printf(" ----------------------------------------------------------------------------- \n");
		printf(" Ingrese codigo del producto             ==> [    ]\b\b\b\b\b");
		scanf("%d", &tempCode);
		encontrar = VerificarExistencia(prod,tempCode);
		if(encontrar!=-1){
			printf("\n El codigo %04 ya esta siendo usado por otro producto.",
					tempCode);
			fflush(stdin);getchar();
		}
	}while(encontrar!=-1);
	
	prod[idprod].codigo = tempCode;
	fflush(stdin);
	printf(" Ingrese el nombre del producto          ==>  ");
	gets(prod[idprod].nombre);
	fflush(stdin);
	printf(" Ingrese descripcion del producto        ==>  ");
	gets(prod[idprod].descripcion);
	fflush(stdin);
	printf(" Ingrese la clasificacion del producto   ==>  ");
	gets(prod[idprod].categoria);
	fflush(stdin);
	printf(" Ingrese la cantidad incial del producto ==>  ");
	scanf("%d",&prod[idprod].invInicial);
	(prod[idprod].invActual)=(prod[idprod].invInicial);
	fflush(stdin);
	printf(" Ingrese su nombre y apellido            ==>  ");
	gets(prod[idprod].usuario);
	fflush(stdin);
	printf(" ----------------------------------------------------------------------------- \n");
	SystemHour(&prod[idprod].tiempo);
	printf(" Fecha y Hora de Ingreso del Producto  %04d:  %02d/%02d/%02d  %02d:%02d\n",
			prod[idprod].codigo,
			prod[idprod].tiempo.dia,
			prod[idprod].tiempo.mes,
			prod[idprod].tiempo.anio,
			prod[idprod].tiempo.hora,
			prod[idprod].tiempo.minuto);
			
	GenerarTxIngreso(trans,prod,idprod,idtx,tempCode);
	
	fflush(stdin);
	getchar();
}

void GenerarTxIngreso(struct TRANSACCIONES *trans,struct PRODUCTOS *prod,int idprod,int idtx,int tempCode){
	trans[idtx].idtx        = idtx+1;
	trans[idtx].codigo      = tempCode;
	trans[idtx].tipo        = 1;
	trans[idtx].invAnterior = 0;
	trans[idtx].cantidad    = prod[idprod].invActual;
	trans[idtx].invActual   = prod[idprod].invActual;
	strcpy(trans[idtx].usuario,prod[idprod].usuario);
	trans[idtx].tiempo      = prod[idprod].tiempo;
}

void MetodoBurbuja(struct PRODUCTOS *productos,int idprod){
	struct PRODUCTOS auxProd;
		
	for(int i=1; i<idprod; i++){
		for(int j=0; j<idprod-1; j++){
			if (productos[j].codigo > productos[j+1].codigo){
				auxProd        = productos[j+1];
				productos[j+1] = productos[j];
				productos[j]   = auxProd;
			}
		}
	}
}

void MostrarDetalles(struct PRODUCTOS *productos){
	int tempCode=0,encontrar=-1;;
	
	do{
		system("cls");
		printf("\n                        ==> DETALLES DE PRODUCTOS <==                        \n");
		printf(" ----------------------------------------------------------------------------- \n");
		printf(" Digite el codigo del producto:   [    ]\b\b\b\b\b");
		scanf("%d",&tempCode);
		encontrar=VerificarExistencia(productos,tempCode);
		if(encontrar==-1){
			printf("\n El codigo no existe, vuelva a intentarlo...");
			fflush(stdin);getchar();
		}
	}while(encontrar==-1);
	
	system("cls");
	printf("\n                        ==> DETALLES DE PRODUCTO <==                           \n");
	printf(" ----------------------------------------------------------------------------- \n");
	printf(" Codigo de Producto:                    %04d\n",productos[encontrar].codigo);
	printf(" Nombre de Producto:                    %s\n",productos[encontrar].nombre);
	printf(" Descripcion del producto:              %s\n",productos[encontrar].descripcion);
	printf(" Categoria del producto:                %s\n",productos[encontrar].categoria);
	printf(" Cantidad inicial del producto:         %d\n",productos[encontrar].invInicial);
	printf(" Cantidad actual del producto:          %d\n",productos[encontrar].invActual);
	printf(" Usuario que ingreso el producto:       %s\n",productos[encontrar].usuario);
	printf(" Fecha y Hora de creacion del producto: %02d/%02d/%04d   %02d:%02d\n",
			productos[encontrar].tiempo.dia,
			productos[encontrar].tiempo.mes,
			productos[encontrar].tiempo.anio,
			productos[encontrar].tiempo.hora,
			productos[encontrar].tiempo.minuto);
	fflush(stdin);getchar();
	
}

void MostrarInventario (struct PRODUCTOS *prod){
	system("cls");
	printf("\n                    ==> LISTADO DE PRODUCTOS REGISTRADOS <==                   \n");
	printf(" ----------------------------------------------------------------------------- \n");
	printf("   %04s\t%-25s%-15s%20s\n", "CODIGO", "NOMBRE", "CATEGORIA", "CANTIDAD ACTUAL");
	printf(" ----------------------------------------------------------------------------- \n");
	for (int i = 0; i < MAXPRODUCTS; i++) {
		if(prod[i].codigo!=-1){
			printf("     %04d       %-25s%-20s%15d\n",
					prod[i].codigo,
					prod[i].nombre,
					prod[i].categoria,
					prod[i].invActual);
		}
				
	}
	printf(" ----------------------------------------------------------------------------- \n");
	fflush(stdin);
	getchar();
}

void Transaccion(struct PRODUCTOS *productos,struct TRANSACCIONES *transacciones,int idtx){
	int tx=0,temp_code=0,encontrar=-1,error=0;
	do{
		system("cls");
		printf("\n                   ==> MENU DE REGISTRO DE MOVIMIENTOS <==                      ");
		printf(" ----------------------------------------------------------------------------- \n");					
		printf(" Ingrese el codigo del producto:    ");
		fflush(stdin);
		scanf("%d",&temp_code);
		encontrar=VerificarExistencia(productos,temp_code);
		if(encontrar==-1){
			printf("Codigo incorrecto, vuelva a intentarlo...");
			fflush(stdin);getchar();
		}
	}while(encontrar==-1);
	printf("\n Producto:                          %s",productos[encontrar].nombre);
	printf("\n Cantidad de existencias actual:    %d",productos[encontrar].invActual);
	printf("\n\n Seleccione el tipo de movimiento a registrar:\n\n");
	printf("   1. Ingreso de Productos\n");
	printf("   2. Egreso de Productos\n");
	do{
		printf("\n Opcion: [ ]\b\b");
		scanf("%d",&tx);
	}while(tx<1||tx>2);
	transacciones[idtx].invAnterior=productos[encontrar].invActual;
	do{
		printf(" Ingrese la cantidad de productos usados en el movimiento: ");
		if(tx==2){
			printf("(-) ");
		}
		scanf("%d",&transacciones[idtx].cantidad);
		
		if((tx==2) && (transacciones[idtx].cantidad>transacciones[idtx].invAnterior)){
			error=1;
			printf("\n Usted posee productos insucicientes para realizar la transaccion,\n vuelva a intentarlo...\n");
			fflush(stdin);
			getchar();
		}else{
			error=0;
		}
	}while(error==1);
		
	transacciones[idtx].idtx=idtx+1;
	transacciones[idtx].codigo=temp_code;
	transacciones[idtx].tipo=tx;
				
	switch(tx){
		case 1://Ingreso
			transacciones[idtx].invActual=transacciones[idtx].invAnterior+transacciones[idtx].cantidad;
			break;	
		case 2://Egreso
			transacciones[idtx].invActual=transacciones[idtx].invAnterior-transacciones[idtx].cantidad;
			break;
	}
		
	productos[encontrar].invActual=transacciones[idtx].invActual;
	
	printf(" Ingrese el usuario que realiza la transaccion:            ");
	fflush(stdin);
	gets(transacciones[idtx].usuario);
	printf("\n Existencias despues del registro de transaccion:          %d\n",productos[encontrar].invActual);
	printf(" ----------------------------------------------------------------------------- \n");
	SystemHour(&transacciones[idtx].tiempo);
	printf(" Fecha y Hora de la Transaccion    %04d:  %02d/%02d/%02d  %02d:%02d\n",
			transacciones[idtx].codigo,
			transacciones[idtx].tiempo.dia,
			transacciones[idtx].tiempo.mes,
			transacciones[idtx].tiempo.anio,
			transacciones[idtx].tiempo.hora,
			transacciones[idtx].tiempo.minuto);
	
	
	fflush(stdin);
	getchar();
}

 void MenuReporte(struct TRANSACCIONES *transacciones,struct PRODUCTOS *productos,int idtx){
	int opcion=0;
	do{
		system("cls");
		printf("\n                   ==> REPORTE DE TRANSACCIONES REALIZADAS <==                 \n");
		printf(" ----------------------------------------------------------------------------- \n");
		printf("  Seleccione el tipo de Reporte a generar:\n\n");
		printf("  1. Todos los reportes.\n");
		printf("  2. Mostrar Reportes por Fecha\n");
		printf("  3. Mostrar Reportes por Fecha y Producto\n");
		printf("  4. Regresar al Menu Principal\n\n");
		printf("  Opcion:  [ ]\b\b");
		scanf("%d",&opcion);
	}while(opcion<1 || opcion>4);
	
	switch(opcion){
		case 1:
			ReporteTxTodos(transacciones,idtx);
			break;
		case 2:
			ReporteTxFecha(transacciones,idtx);
			break;
		case 3:
			ReporteTxFechaProd(transacciones,productos,idtx);
			break;
	}
}

void ReporteTxTodos(struct TRANSACCIONES *transacciones,int idtx){
	struct TIEMPO tiempoReg1;
	SystemHour(&tiempoReg1);
	system("cls");
	printf("\n                           ==> TODOS LOS REPORTES <==                          \n");
	printf(" ----------------------------------------------------------------------------- \n");
	printf("  ID  COD.  TIPO  INV.ANT.  CANT.  INV.ACT.  USUARIO          FECHA Y HORA\n");
	printf(" ----------------------------------------------------------------------------- \n");
	for(int i=0;i<idtx;i++){
		printf("  %02d  %04d   %02d      %4d    %3d       %3d   %-15s %02d/%02d/%04d %02d:%02d\n",
				transacciones[i].idtx,
				transacciones[i].codigo,
				transacciones[i].tipo,
				transacciones[i].invAnterior,
				transacciones[i].cantidad,
				transacciones[i].invActual,
				transacciones[i].usuario,
				transacciones[i].tiempo.dia,
				transacciones[i].tiempo.mes,
				transacciones[i].tiempo.anio,
				transacciones[i].tiempo.hora,
				transacciones[i].tiempo.minuto);
	}
	printf(" ----------------------------------------------------------------------------- \n");
	printf("  Fecha y Hora del Registro:  %02d/%02d/%04d %02d:%02d",
			tiempoReg1.dia,
			tiempoReg1.mes,
			tiempoReg1.anio,
			tiempoReg1.hora,
			tiempoReg1.minuto);
	fflush(stdin);
	getchar();
}

void PedirFecha(int fecha[]){
	printf("  Ingrese la fecha a mostrar en el reporte: \n");
	do{
		printf("  Dia:   [  ]\b\b\b");
		scanf("%d",&fecha[0]);
		if(fecha[0]<1||fecha[0]>31){
			printf("  Dia invalido...");
			fflush(stdin);
			getchar();
		}
	}while(fecha[0]<1||fecha[0]>31);
	do{
		printf("  Mes:   [  ]\b\b\b");
		scanf("%d",&fecha[1]);
		if(fecha[1]<1||fecha[1]>12){
			printf("  Mes invalido...");
			system("pause");
		}
	}while(fecha[1]<1||fecha[1]>12);
	do{
		printf("  Anio:  [    ]\b\b\b\b\b");
		scanf("%d",&fecha[2]);
		if(fecha[0]<1){
			printf("  Anio invalido...");
			fflush(stdin);
			getchar();
		}
	}while(fecha[0]<1);
}

void ReporteTxFecha(struct TRANSACCIONES *transacciones,int idtx){
	int fecha[3];
	struct TIEMPO tiempoReg2;
	SystemHour(&tiempoReg2);
	system("cls");
	printf("\n                           ==> REPORTES POR FECHA <==                          \n");
	printf(" ----------------------------------------------------------------------------- \n");
	
	PedirFecha(fecha);
	
	system("cls");
	printf("\n                     ==> REPORTES DE FECHA: %02d/%02d/%04d <==                    \n",
			fecha[0],
			fecha[1],
			fecha[2]);
	printf(" ----------------------------------------------------------------------------- \n");
	printf("  ID  COD.  TIPO  INV.ANT.  CANT.  INV.ACT.  USUARIO          FECHA Y HORA\n");
	printf(" ----------------------------------------------------------------------------- \n");
	for(int i=0;i<idtx;i++){
		if( transacciones[i].tiempo.dia==fecha[0] &&
			transacciones[i].tiempo.mes==fecha[1] &&
			transacciones[i].tiempo.anio==fecha[2]){
			printf("  %02d  %04d   %02d      %4d    %3d       %3d   %-15s %02d/%02d/%04d %02d:%02d\n",
					transacciones[i].idtx,
					transacciones[i].codigo,
					transacciones[i].tipo,
					transacciones[i].invAnterior,
					transacciones[i].cantidad,
					transacciones[i].invActual,
					transacciones[i].usuario,
					transacciones[i].tiempo.dia,
					transacciones[i].tiempo.mes,
					transacciones[i].tiempo.anio,
					transacciones[i].tiempo.hora,
					transacciones[i].tiempo.minuto);
		}
	}
	printf(" ----------------------------------------------------------------------------- \n");
	printf("  Fecha y Hora del Registro:  %02d/%02d/%04d %02d:%02d",
			tiempoReg2.dia,
			tiempoReg2.mes,
			tiempoReg2.anio,
			tiempoReg2.hora,
			tiempoReg2.minuto);
	fflush(stdin);
	getchar();
}

void ReporteTxFechaProd(struct TRANSACCIONES *transacciones,struct PRODUCTOS *productos,int idtx){
	int fecha[3],code,encontrar;
	struct TIEMPO tiempoReg3;
	SystemHour(&tiempoReg3);
	do{
		system("cls");
		printf("\n                     ==> REPORTES POR FECHA Y PRODUCTO <==                     \n");
		printf(" ----------------------------------------------------------------------------- \n");
		printf(" Digite el codigo del producto:   [    ]\b\b\b\b\b");
		scanf("%d",&code);
		encontrar=VerificarExistencia(productos,code);
		if(encontrar==-1){
			printf("\n El codigo no existe, vuelva a intentarlo...");
			fflush(stdin);
			getchar();
		}
	}while(encontrar==-1);
	
	PedirFecha(fecha);
	
	system("cls");
	printf("\n               ==> REPORTES DE PRODUCTO %04d FECHA: %02d/%02d/%04d <==             \n",
			code,
			fecha[0],
			fecha[1],
			fecha[2]);
	printf(" ----------------------------------------------------------------------------- \n");
	printf("  ID  COD.  TIPO  INV.ANT.  CANT.  INV.ACT.  USUARIO          FECHA Y HORA\n");
	printf(" ----------------------------------------------------------------------------- \n");
	for(int i=0;i<idtx;i++){
		if( transacciones[i].tiempo.dia==fecha[0] &&
			transacciones[i].tiempo.mes==fecha[1] &&
			transacciones[i].tiempo.anio==fecha[2] &&
			transacciones[i].codigo==code){
			printf("  %02d  %04d   %02d      %4d    %3d       %3d   %-15s %02d/%02d/%04d %02d:%02d\n",
					transacciones[i].idtx,
					transacciones[i].codigo,
					transacciones[i].tipo,
					transacciones[i].invAnterior,
					transacciones[i].cantidad,
					transacciones[i].invActual,
					transacciones[i].usuario,
					transacciones[i].tiempo.dia,
					transacciones[i].tiempo.mes,
					transacciones[i].tiempo.anio,
					transacciones[i].tiempo.hora,
					transacciones[i].tiempo.minuto);
		}
	}
	printf(" ----------------------------------------------------------------------------- \n");
	printf("  Fecha y Hora del Registro:  %02d/%02d/%04d %02d:%02d",
			tiempoReg3.dia,
			tiempoReg3.mes,
			tiempoReg3.anio,
			tiempoReg3.hora,
			tiempoReg3.minuto);
	fflush(stdin);
	getchar();
}

void Creditos(){
	system("cls");
	printf("\n       ******************************************************************      ");
	printf("\n       *                 GRACIAS POR USAR EL PROGRAMA                   *      ");                  
	printf("\n       *               PROYECTO - SISTEMA DE INVENTARIO                 *      ");
	printf("\n       *                          \"CHIDORI\"                             *     "); 
	printf("\n       *                                                                *      ");
	printf("\n       *                  INTEGRANTES DEL GRUPO:                        *      ");
	printf("\n       *                  - DIAZ CAMPOVERDE BRYAN                       *      ");
	printf("\n       *                  - GAIBOR OCHOA JORGE                          *      ");
	printf("\n       *                  - LAZO VILLANUEVA ANDRE                       *      ");
	printf("\n       *                  - MAQUILON CHACHA VICTOR                      *      ");
	printf("\n       *                                                                *      ");
	printf("\n       *            PROGRAMACION II          ISI-S-MA-2-3               *      ");
	printf("\n       ******************************************************************      ");
}

